import game.GameMode;
import game.GameStatus;
import game.GuessTheNumberGame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by emaksimovich on 30.01.17.
 */
public class Server {

    private static int PORT_NUMBER = 56789;
    private static ServerSocket serverSocket;

    private class SocketListener extends Thread {

        public SocketListener() {
            start();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    new GameSession(serverSocket.accept());
                    System.out.println("New client connected");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private class GameSession extends Thread {

        private Socket clientSocket;
        private PrintWriter writer;
        private BufferedReader reader;

        public GameSession(Socket clientSocket) {
            this.clientSocket = clientSocket;
            try {
                writer = new PrintWriter(clientSocket.getOutputStream(), true);
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            start();
        }

        @Override
        public void run() {
            writer.println("Welcome to Guess the Number game");
            writer.println("Choose your game mode. 1 to 100 (1) / 1 to 1000 (2): ");
            int result = 1;
            String str;
            try {
                while ((str = reader.readLine()) == null);
                result = Integer.parseInt(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
            GameMode gameMode = GameMode.values()[result - 1];
            GuessTheNumberGame guessTheNumberGame = new GuessTheNumberGame(gameMode);
            writer.println("I guessed a number. Try to guess. You have " + guessTheNumberGame.getGuessCount() + " tries: ");
            while (true) {
                int number;
                try {
                    while ((str = reader.readLine()) == null);
                    number = Integer.parseInt(str);
                    GameStatus status = guessTheNumberGame.guess(number);
                    if (status == GameStatus.LESS) writer.println("Nice try, but guessed number is lesser. You still have " + guessTheNumberGame.getGuessCount() + " tries: ");
                    if (status == GameStatus.MORE) writer.println("Nice try, but guessed number is bigger. You still have " + guessTheNumberGame.getGuessCount() + " tries: ");
                    if (status == GameStatus.LOSE) {
                        writer.println("Sorry, you loose. I guessed " + guessTheNumberGame.getPuzzledNumber());
                        break;
                    }
                    if (status == GameStatus.WIN) {
                        writer.println("Congrat! You won!");
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            try {
                writer.println("Bye!");
                reader.close();
                writer.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public Server() {
        try {
            serverSocket = new ServerSocket(PORT_NUMBER);
            new SocketListener();
            System.out.println("Server started @ port " + PORT_NUMBER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Server server = new Server();
    }

}
