package game;

import java.util.Random;

/**
 * Created by emaksimovich on 30.01.17.
 */
public class GuessTheNumberGame {

    private int puzzledNumber;
    private int guessCount;

    public int getPuzzledNumber() {
        return puzzledNumber;
    }

    public int getGuessCount() {
        return guessCount;
    }

    public GuessTheNumberGame(GameMode gameMode) {
        Random random = new Random();
        if (gameMode == GameMode.MODE_1TO100) {
            puzzledNumber = random.nextInt(100) + 1;
            guessCount = 7;

        }
        if (gameMode == GameMode.MODE_1TO1000) {
            puzzledNumber = random.nextInt(1000) + 1;
            guessCount = 9;
        }
    }

    public GameStatus guess(int number) {
        if (--guessCount < 1) return GameStatus.LOSE;
        if (number > puzzledNumber) return GameStatus.LESS;
        if (number < puzzledNumber) return GameStatus.MORE;
        return GameStatus.WIN;
    }

}
