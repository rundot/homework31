package game;

/**
 * Created by emaksimovich on 30.01.17.
 */
public enum GameStatus {
    MORE,
    LESS,
    WIN,
    LOSE;
}
