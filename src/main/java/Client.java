import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by emaksimovich on 30.01.17.
 */
public class Client {

    private static final String SERVER_HOST = "localhost";
    private static final int SERVER_PORT = 56789;
    private Socket serverSocket;
    private BufferedReader reader;
    private PrintWriter writer;

    public String askServer() {
        String result = null;
        try {
            //Waiting for server input
            while ((result = reader.readLine()) == null) ;
            //Reading till the end of input
            while (reader.ready()) result += "\n" + reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Client() {
        try {
            serverSocket = new Socket(SERVER_HOST, SERVER_PORT);
            reader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
            writer = new PrintWriter(serverSocket.getOutputStream(), true);
            BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
            String message;
            while (!(message = askServer()).contains("Bye!")) {
                System.out.println(message);
                writer.println(consoleReader.readLine());
            }
            System.out.println(message);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
                writer.close();
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        Client client = new Client();
    }
}
